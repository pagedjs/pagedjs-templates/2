---
author: Alice Ricci
title: Colmar 
project: paged.js sprint
book format: 6in 9in
book orientation: portrait
---



## Typefaces
### Fivo Sans Modern
Design by Alex Slobzheninov
https://www.behance.net/gallery/54442585/FIvo-Sans-Modern-Free-Display-Typeface
SIL Open Font License




## Supported tags
See issue
